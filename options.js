
if (typeof(browser) === "undefined") { browser = chrome; }


function saveOptions (event) {
    event.preventDefault();

    let options = {
        isActive      : document.querySelector("#option_isActive").checked,
        logToConsole  : document.querySelector("#option_logToConsole").checked,
        consentCookie : document.querySelector("#option_consentCookie").value,
        preferred_hl  : document.querySelector("#option_preferred_hl").value
    };

    browser.storage.sync
        .set({ options: options },  // .set(...).then(res => ...)
             res => { browser.runtime.sendMessage({ type: "anti-googleSearch - optionsChanged", options: options }) });
}


function loadOptions () {
    browser.storage.sync.get(["options"], res => {  // browser.storage.sync.get(["options"]).then(res => {
        let options = res.options;
        document.querySelector("#option_isActive"     ).checked = res.options.isActive;
        document.querySelector("#option_logToConsole" ).checked = res.options.logToConsole;
        document.querySelector("#option_consentCookie").value   = res.options.consentCookie;
        document.querySelector("#option_preferred_hl" ).value   = res.options.preferred_hl;
    });
}


document.addEventListener("DOMContentLoaded", loadOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
