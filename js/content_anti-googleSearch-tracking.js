
// Cleanup the googleSearch page.
//
// 'content' scope

if (typeof(browser) === "undefined") { browser = chrome; }


function gs_clean (options) {
    if (!options.isActive) { return { cancel: false }; }

    let isGoogleUrl = new RegExp('\\b(google|gmail|googleapis)\\b').test(window.location.href);
    if (!isGoogleUrl) { return { cancel: false }; }

    jQuery("a").each((idx,a) => { a.removeAttribute("onmousedown"); });

    jQuery("script"    ).each((idx,elt) => { elt.attributes; });
    jQuery("noscript"  ).each((idx,elt) => { elt.parentNode.removeChild(elt); });

    jQuery("#bottomads").each((idx,elt) => { elt.parentNode.removeChild(elt); });
    jQuery("#botstuff" ).each((idx,elt) => { elt.parentNode.removeChild(elt); });
    jQuery("#brs"      ).each((idx,elt) => { elt.parentNode.removeChild(elt); });
    jQuery("#footcnt"  ).each((idx,elt) => { elt.parentNode.removeChild(elt); });
    jQuery("#rhs"      ).each((idx,elt) => { elt.parentNode.removeChild(elt); });
}


async function browser__main () {
    let options = await browser.runtime.sendMessage({ type: "anti-googleSearch - getOptions" })
    gs_clean(options);
}
function chrome__main () {
    browser.runtime.sendMessage({ type: "anti-googleSearch - getOptions" }, res => {
        let options = res;
        gs_clean(options);
    })
}

__callF(this,"main");
