
// Prevent googleSearch tracking. Many google feature will not work any more...
//
// 'background' scope

console.log("[Anti-GS] anti-googleSearch-tracking extension -- Background");

if (typeof(browser) === "undefined") { browser = chrome; }


let default_options = {
    isActive      : true,
    logToConsole  : true,
    consentCookie : "YES+EN",  // no trailing ';'
    preferred_hl  : ""         // language to use if 'hl' not specified in the url (Ex: "en"). Can be empty.
};

let options_cached = default_options;

function onOptionsChanged () {
    let icon_path = options_cached.isActive
        ? "icons/Anti-GoogleSearch_active.svg"
        : "icons/Anti-GoogleSearch_inactive.svg";
    browser.browserAction.setIcon({
        path : { "16": icon_path, "24": icon_path, "32": icon_path, "48": icon_path, "72": icon_path, "96": icon_path, "128": icon_path }
    });
}

browser.storage.sync.get(["options"], res => {  // browser.storage.sync.get(["options"]).then(res => {
    if (isEmptyObject(res)) { // first run (=> initialize options)
        console.log("[Anti-GS] Initialize options (first run)");
        browser.storage.sync.set(
            { options: default_options },
            () => onOptionsChanged()
        );
    }
    else {
        options_cached = res.options;
        onOptionsChanged();
    }
});


let handleMessage_options_getOptions = __get(
    (message, sender, sendResponse) => { return Promise.resolve(options_cached); },
    (message, sender, sendResponse) => { sendResponse(options_cached); return false; /* sync call (use "return true;" for async) */ }
)


//https://github.com/uncommonhacks/webextension-starter/blob/master/background/background.js
function handleMessage_options (message, sender, sendResponse) {
    if (message.type === "anti-googleSearch - getOptions") {
        return handleMessage_options_getOptions(message, sender, sendResponse);
    }
    else if (message.type === "anti-googleSearch - optionsChanged") {
        options_cached = message.options;
        console.log("%s: %O", "[Anti-GS] Options saved", options_cached);
        onOptionsChanged();
    }
}
browser.runtime.onMessage.addListener(handleMessage_options);



let gs_originRootUrl_regex     = new RegExp('^https?://(www\\.)?google\\.([a-z.]+)/?(\\?.*)?$');  // => match https://google.xx/?q=myQuery
let gs_originSearchUrl_regex   = new RegExp('^https?://(www\\.)?google\\.([a-z.]+)/search\\?');
let gs_setPreferencesUrl_regex = new RegExp('^https?://(www\\.)?google\\.([a-z.]+)/(preferences|setprefs)\\?');
let gs_autocompleteUrl_regex   = new RegExp('^https?://(www\\.)?google\\.([a-z.]+)/complete/search\\?');
let gs_xjsUrl_regex            = new RegExp('^https?://(www\\.)?google\\.([a-z.]+)/xjs/');
let gs_gen204Url_regex         = new RegExp('^https?://(www\\.)?google\\.([a-z.]+)/gen_204\\?');


function f_onBeforeRequest_listener (requestDetails) {
    return f_onBeforeRequest_aux(options_cached, requestDetails);
}


function f_onBeforeSendHeaders_listener (requestDetails) {
    return f_onBeforeSendHeaders_aux(options_cached, requestDetails);
}


browser.webRequest.onBeforeRequest.addListener(
    f_onBeforeRequest_listener,
    { urls: ["<all_urls>"] },
    [ "blocking" ]
);

browser.webRequest.onBeforeSendHeaders.addListener(
    f_onBeforeSendHeaders_listener,
    { urls: ["<all_urls>"] },
    __get(
        [ "blocking", "requestHeaders" ],                 // "extraHeaders" is required for chrome,
        [ "blocking", "requestHeaders", "extraHeaders" ]  // but raises an error in firefox
    )
);



function f_onBeforeRequest_aux (options, requestDetails) {
    if (!options.isActive) { return { /*cancel: false*/ }; }

    let url       = requestDetails.url;
    let method    = requestDetails.method;
    let type      = requestDetails.type;
    let originUrl = requestDetails.originUrl || requestDetails.initiator;  // originUrl is firefox, initiator is chrome

    let isGoogleSearchRequest   = gs_originSearchUrl_regex.test(url);
    let isFromGoogleSearchPage  = gs_originRootUrl_regex.test(originUrl) || gs_originSearchUrl_regex.test(originUrl);
    let fIsWhitelistQueryParam  = (queryParamName => ["q", "hl", "start", "tbm", "tbs"].includes(queryParamName));
    let isSetPreferencesRequest = gs_setPreferencesUrl_regex.test(url);

    // search url => cleanup query parameters
    if (isGoogleSearchRequest) {
        let urlModified_languageAdded = false;
        let urlModified_cleaned       = false;

        let queryParams = URI.parseQuery(URI.parse(url).query);

        if (typeof(queryParams["hl"]) === "undefined" && options.preferred_hl.length > 0) {
            queryParams["hl"] = options.preferred_hl;  // language not specified in URL => set preferred language
            urlModified_languageAdded = true;
        }

        let cleanedQueryParams = Object.filter(queryParams, (queryParamEntry => {
            let queryParamName  = queryParamEntry[0];
            let queryParamValue = queryParamEntry[1];
            return fIsWhitelistQueryParam(queryParamName);
        }));

        let nbQueryParamsNames_orig  = Object.keys(       queryParams).length;
        let nbQueryParamsNames_clean = Object.keys(cleanedQueryParams).length;
        urlModified_cleaned = (nbQueryParamsNames_clean != nbQueryParamsNames_orig);

        if (urlModified_cleaned) {
            let cleaned_url = URI(url).query(URI.buildQuery(cleanedQueryParams)).toString();
            if (options.logToConsole) { console.log(`[Anti-GS] Remove googleSearch non essential query parameters (${url} => ${cleaned_url})`); }
            return { redirectUrl: cleaned_url };  // Do NOT add "cancel: false", as under chrome, it cancels the 'redirectUrl'.
        }
        else if (urlModified_languageAdded) {
            let urlWithLanguage = URI(url).query(URI.buildQuery(queryParams)).toString();
            if (options.logToConsole) { console.log(`[Anti-GS] Add the language query parameter (${url} => ${urlWithLanguage})`); }
            return { redirectUrl: urlWithLanguage };  // Do NOT add "cancel: false", as under chrome, it cancels the 'redirectUrl'.
        }
        else {  // let pass it as-it-is
            return { /*cancel: false*/ };
        }
    }

    // otherwise, hack (only) requests coming from google search landing page or search result page
    else if (isFromGoogleSearchPage) {

        if (isSetPreferencesRequest) {
            let old_queryParams = URI.parseQuery(URI.parse(originUrl).query);
            let new_queryParams = URI.parseQuery(URI.parse(      url).query);
            let old_hl = old_queryParams["hl"];
            let new_hl = new_queryParams["hl"];
            if (typeof(new_hl) !== "undefined" && new_hl != old_hl) {
                let old_url_with_hl = URI(originUrl).query(URI.buildQuery({"hl":new_hl})).toString();
                return { redirectUrl: old_url_with_hl };  // Do NOT add "cancel: false", as under chrome, it cancels the 'redirectUrl'.
            }
            else {
                return { cancel: true };  // cancel the request. This is a questionable decision as quite intrusive.
            }
        }

        if (type != "main_frame") {
            // discard type "beacon", "image", "other", "script", "xmlhttprequest", ...
            if (options.logToConsole) { console.log(`[Anti-GS] Block url [is type '${type}']: ${url}`); }
            return { cancel: true };
        }
        if (gs_autocompleteUrl_regex.test(url)) {
            if (options.logToConsole) { console.log(`[Anti-GS] Block url [is autocomplete]: ${url}`); }
            return { cancel: true };
        }
        if (gs_xjsUrl_regex.test(url)) {
            if (options.logToConsole) { console.log(`[Anti-GS] Block url [is '/xjs/']: ${url}`); }
            return { cancel: true };
        }
        if (gs_gen204Url_regex.test(url)) {
            if (options.logToConsole) { console.log(`[Anti-GS] Block url [is '/gen204/']: ${url}`); }
            return { cancel: true };
        }
        if (method != "GET") {
            if (options.logToConsole) { console.log(`[Anti-GS] Block url [is method '${method}' (not GET)]: ${url}`); }
            return { cancel: true };
        }
    }

    return {
        /*cancel: false*/
    };

}



function f_onBeforeSendHeaders_aux (options, requestDetails) {
    if (!options.isActive) { return { /*cancel: false*/ }; }

    let url       = requestDetails.url;
    let originUrl = requestDetails.originUrl || requestDetails.initiator;  // originUrl is firefox, initiator is chrome

    let isGoogleSearchPage      = gs_originRootUrl_regex.test(url) || gs_originSearchUrl_regex.test(url);
    let isGoogleSearchRequest   = gs_originSearchUrl_regex.test(url);
    let isFromGoogleSearchPage  = gs_originRootUrl_regex.test(originUrl) || gs_originSearchUrl_regex.test(originUrl);

    let isGoogle = isGoogleSearchPage || isGoogleSearchRequest || isFromGoogleSearchPage;

    if (isGoogle) {
        if (options.logToConsole) { console.log(`[Anti-GS] Hack Google cookies for URL: ${url}`); }

        let newRequestHeaders = requestDetails.requestHeaders.filter((requestHeader, idx) => {
            let headerNameOrig      = requestHeader.name;
            let headerNameLowercase = requestHeader.name.toLowerCase();
            if (false) {}
            else if (headerNameLowercase === "origin"    ) { return false; }
            else if (headerNameLowercase === "referer"   ) { return false; }
            else if (headerNameLowercase === "cookie"    ) { return false; }
            else if (headerNameLowercase.startsWith("x-")) { return false; }  // e.g. x-client-data: COqSywE= Decoded: message ClientVariations { /* Active client experiment variation IDs. */ repeated int32 variation_id = [1234567]; }
            else { return true; }
        });

        newRequestHeaders.push({ "name": "Cookie", "value": `CONSENT=${options.consentCookie};` });

        return { requestHeaders: newRequestHeaders };  // Do NOT add "cancel: false", as under chrome, it cancels the 'redirectUrl'.
    }

    return {
        /*cancel: false*/
    };

}
