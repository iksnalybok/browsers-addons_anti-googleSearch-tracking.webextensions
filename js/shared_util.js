
// https://stackoverflow.com/questions/679915/how-do-i-test-for-an-empty-javascript-object#answer-32108184
function isEmptyObject (obj) {
    return Object.entries(obj).length === 0 && obj.constructor === Object;
}


Object.filter = (obj, predicate) => Object.fromEntries(Object.entries(obj).filter(predicate));


async function sleep (durationMillis) {
    await new Promise(res => setTimeout(res, durationMillis));  // sleep
}


const isFirefox1 = !chrome.app;
const isFirefox2 = typeof(browser) !== 'undefined' && !!browser.runtime;
const isBrowser  = isFirefox1;
const isChrome   = !isBrowser;


function __get (browser_arg, chrome_arg) {
    if (isBrowser) { return browser_arg; }
    if (isChrome ) { return  chrome_arg; }
}


// adapted from https://stackoverflow.com/questions/359788/how-to-execute-a-javascript-function-when-i-have-its-name-as-a-string
function __getF (context, fullFuncName) {
    let namespaces = fullFuncName.split(".");
    let baseFuncName = namespaces.pop();
    for (let i = 0; i < namespaces.length; i++) {
        context = context[namespaces[i]];
    }
    let func = null;
    try { if (func == null && isBrowser) { func = context["browser__"+baseFuncName]; } }catch (ex) {}
    try { if (func == null && isChrome ) { func = context["chrome__" +baseFuncName]; } }catch (ex) {}
    try { if (func == null             ) { func = context[            baseFuncName]; } }catch (ex) {}
    return func;
}

function __callF (context, fullFuncName /*, args */) {
    let args = Array.prototype.slice.call(arguments, 2);
    let func = __getF(context, fullFuncName);
    if (func != null && typeof(func) === "function") {
        return func.apply(context, args);
    }else {
        console.error("Function not found: '"+fullFuncName+"'.", typeof(func), context);
    }
}
