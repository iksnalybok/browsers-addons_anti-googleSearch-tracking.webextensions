
XPI_NAME := "anti-googleSearch-tracking"

all: xpi

xpi: license icons
	$(eval XPI_VERSION          := $(shell cat manifest.json | sed -n '/"version":/p' | sed 's+^[^:]*: *"\([^"]*\)" *, *+\1+'))
	$(eval XPI_FILENAME_VERSION := $(shell echo "$(XPI_NAME)-$(XPI_VERSION)-unsigned.xpi"))
	$(eval XPI_FILENAME_LAST    := $(shell echo "$(XPI_NAME)-unsigned.xpi"))
	if [ -f "../$(XPI_FILENAME_VERSION)" ]; then rm -v "../$(XPI_FILENAME_VERSION)"; fi
	find . -path './.git*' -prune -o -path ./devonly -prune -o -type f ! -name '*~' -print | sort | zip ../$(XPI_FILENAME_VERSION) -@
	if [ -h "../$(XPI_FILENAME_LAST)" ]; then rm -v "../$(XPI_FILENAME_LAST)"; fi;
	cd .. ; ln -v -s "$(XPI_FILENAME_VERSION)" "$(XPI_FILENAME_LAST)"


iconSizes := 16 24 32 48 72 96 128

icons: $(iconSizes) svg

svg: icons/Anti-GoogleSearch_active.svg icons/Anti-GoogleSearch_inactive.svg

$(iconSizes) : % : icons/Anti-GoogleSearch_active_%.png icons/Anti-GoogleSearch_inactive_%.png

icons/Anti-GoogleSearch_active_%.png: devonly/images/Anti-GoogleSearch_active.svg
	@[ -d icons ] || mkdir icons
	$(eval x := $(@:icons/Anti-GoogleSearch_active_%=%))
	$(eval x := $(x:%.png=%))
	inkscape --export-filename=$@ --export-overwrite -w $(x) -h $(x) $<
	exiftool -overwrite_original -all= $@

icons/Anti-GoogleSearch_inactive_%.png: devonly/images/Anti-GoogleSearch_inactive.svg
	@[ -d icons ] || mkdir icons
	$(eval x := $(@:icons/Anti-GoogleSearch_inactive_%=%))
	$(eval x := $(x:%.png=%))
	inkscape --export-filename=$@ --export-overwrite -w $(x) -h $(x) $<
	exiftool -overwrite_original -all= $@


icons/%.svg: devonly/images/%.svg
	cp $^ $@


license: gpl-2.0.txt

gpl-2.0.txt:
	wget -O $@ https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt


devclean:
	-rm -rf icons/


.PHONY: xpi license icons 16 24 32 48 72 96 128 svg devclean
