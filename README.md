# README

This extension removes googleSearch tracking **aggressively**.

- This extension blocks most http requests issued by the
  search page, like non-GET requests, autocomplete, xjs,
  gen204, or requests of type "beacon", "image", "other",
  "script", "xmlhttprequest", etc.\
  The initial motivation for this is that Google monitors events
  (mouseout, mouseover, mousedown, click), and it does it a smart
  way. I could not undo this tracking. The only way I've found to
  get rid of it is to prevent the "xjs" script from running by
  blocking it.\
  A consequence is that **you loose many features**, like typing
  suggestions, "Tools", "Parameters", infinite scrolling, etc.).
  However, it's still possible to run a search!

- This extension removes the non-essential query parameters of a
  search http request. The whitelist ones are: `q`, `hl`, `start`,
  `tbm` and `tbs`.

- Cookies: this extension automatically sets and sends the `CONSENT`
  cookie (to bypass the annoying and slow popup). The value of this
  cookie can be configured in the options (default is "YES+EN").\
  _ALL_ other cookies are removed.

- This extension can set the `hl` query parameter if it is not
  already present in the URL. For this, set the "Preferred 'hl'
  value" option to a non-empty value.\
  It also intercepts the `/preferences` and `/setprefs` URLs,
  extracts the 'hl' value, and puts it back in the original URL.

- This extension cleans the search results links, so you go straight
  to the pages without going through a google redirection (Similar to
  <https://addons.mozilla.org/en-US/firefox/addon/google-search-link-fix/>).


## License

This code is published under the [GPL v2](./gpl-2.0.txt).


## Installation

This extension is unsigned, and not present on the mozilla/google official addons sites.


### Firefox

Use the xpi.

Either use the firefox 'developer' or 'nightly' channels, or
[hack](https://gitlab.com/iksnalybok/browsers-addons_firefox-enable-unsigned-addons)
the firefox installation.

Note: The "extraHeaders" permission in manifest.json makes firefox raises a warning.
  You can safely ignore it: this permission is needed in order this extension works
  without modification under chrome (but it does not work really well).

NB: this is a webextension, it does not work on old firefox versions and forks like palemoon.


### Chromium-based browser

This extension is kind of compatible with chromium-based browsers.

Chrome requires the "extraHeaders" permission to be set, but this breaks stuff, and
some non-google requests fail (like listing an AWS S3 bucket via the web console).
It looks like a bug in the chromuim implementation, as removing that "extraHeaders"
permission solves the issue, but then fail to override the cookies, which is one of
the purposes of this extension. So it works, but breaks other web sites.


Here is a list of such browsers (as this extension is about
privacy, you might be interested in a fork of google-chrome):
  - <https://github.com/Eloston/ungoogled-chromium>
  - <https://www.epicbrowser.com/>
  - <https://www.srware.net/iron/>
  - <https://brave.com/fr/>
  - and more...
      - cf <https://www.zdnet.com/pictures/all-the-chromium-based-browsers/14/>
      - cf <https://en.wikipedia.org/wiki/Chromium_(web_browser)#Active>

Clone this repository (or unzip the xpi), and "Load unpacked" the extension.
Note: you need to enable the "developer mode" in the "More tools -> Extensions" page.
